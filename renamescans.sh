#!/bin/bash
#
# Author: Christian Rohlfing <rohlfing@ient.rwth-aachen.de>

#{{{ Bash settings
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
#}}}


#{{{ Input parameter handling
# Copied from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

usage="$(basename "$0") [-h] [--in infolder] [--csv ] [--out outfolder] --  renames scanned PDFs. 
PDFs in folder 'in' are copied to folder 'out' and renamed according to the entries in 'csv'. 
The number of entries in CSV have to match number of files in 'in'.
Please note that the PDFs in folder 'in' should be named somewhat like this: 'ScanYYYYMMDDHHMMSS.pdf', 
since the script does _not_ look at time stamps but only at the file names!
The PDFs are sorted as in 
find pdfs_scanned/ -type f -name '*.pdf'

Contents in folder 'out' and 'tmp' will be removed!

Options:
    -h, --help      show this help text
    -i, --in        input folder with scanned PDFs. Default: ./pdfs_scanned
    -c, --csv       Moodle grading CSV file, needed to construct the pdf names. Default: ./Bewertungen.csv
    -o, --out       output folder. Default: ./pdfs_renamed
    -d, --dry       flag for dry run, displays only file names.
    -t, --tmp       tmp folder. Default: ./tmp
        --nowarn    disables warnings"

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

OPTIONS=hi:c:o:d,t:v
LONGOPTS=help,in:,csv:,csvsep:,out:,dry,tmp:,nowarn,verbose

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

infolder=- csv=- csvsep=- outfolder=- tmpfolder=- dry=n nowarn=n
# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -h|--help)
            echo "$usage"
            exit
            ;;
        -i|--in)
            infolder="$2"
            shift 2
            ;;
        -c|--csv)
            csv="$2"
            shift 2
            ;;
           --csvsep)
            csvsep="$2"
            shift 2
            ;;
        -o|--out)
            outfolder="$2"
            shift 2
            ;;
        -d|--dry)
            dry=y
            shift 1
            ;;
        -t|--tmp)
            tmpfolder="$2"
            shift 2
            ;;
        --nowarn)
            nowarn=y
            shift 1
            ;;
        -v|--verbose)
            v=y
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Default values
if [[ $infolder = "-" ]]; then
  # global password not given, use distinct password for each pdf
  infolder="./pdfs_scanned"
fi

if [[ $outfolder = "-" ]]; then
  # global password not given, use distinct password for each pdf
  outfolder="./pdfs_renamed"
fi

if [[ $csv = "-" ]]; then
  # global password not given, use distinct password for each pdf
  csv="./Bewertungen.csv"
fi

if [[ $csvsep = "-" ]]; then
  # global password not given, use distinct password for each pdf
  csvsep=","
fi

if [[ $tmpfolder = "-" ]]; then
  # global password not given, use distinct password for each pdf
  tmpfolder="./tmp"
fi
#}}}


# Check folders
for f in \
        "${infolder}" \
        "${outfolder}" \
        "${tmpfolder}"
do
    if ! [ -d "$f" ]; then
        echo "Folder $f does not exist. Exiting."
        exit
    fi
done

if ! [ -f "${csv}" ]; then
    echo "CSV ${csv} does not exist. Exiting."
    exit
fi

# number of lines in CSV
numlines=$(wc -l "${csv}" | awk '{ print (($1-1)) }')

# list of all PDF files
pdffiles=($(find "${infolder}"/ -type f -name "*.pdf" | sort))

# number of all PDF files
numfiles="${#pdffiles[@]}"

# Number of lines in CSV has to match number of PDF files
if ! [[ numlines=numfiles ]]; then
    echo "Number of lines in CSV and number of pdf files are not the same."
    exit
fi

echo
echo "Preparing for renaming"
echo "Processing ${numlines} lines in CSV"


if [[ "$dry" = "y" ]]; then
    echo
    echo "Dry run"
    echo
    dryoutput=""
else
    # Remove files
    rm -rf "${tmpfolder}"/*
    rm -rf "${outfolder}"/*
fi

# Loop over all lines in CSV file
cnt=0

echo -ne "Start iterating..."
{
    read  # skip first row in CSV file since this should be the header
    while read -r line
    do
        # Get current pdf file
        longpdffile="${pdffiles[$cnt]}"
        
        # parse the required fields from the csv file
        id=$(echo $line | awk -F "${csvsep}" '{printf "%s", $1}' | tr -d '"')
        id="${id/"Teilnehmer/in"/}"  # remove "Teilnehmer/in" to get the assignment id

         # remove quotation marks from content
        if [[ "${csvsep}" = ";" ]]; then
            tmp=$(echo $line | awk -F "${csvsep}" '{printf "%s", $2}' | tr -d '"') 
            lastname=$(echo $tmp | awk -F "," '{printf "%s", $1}' | tr -d '"') 
            firstname=$(echo $tmp | awk -F "," '{printf "%s", $2}' | tr -d '"')
            matnum=$(echo $line | awk -F "${csvsep}" '{printf "%s", $3}' | tr -d '"')
        else
            lastname=$(echo $line | awk -F "${csvsep}" '{printf "%s", $2}' | tr -d '"') 
            firstname=$(echo $line | awk -F "${csvsep}" '{printf "%s", $3}' | tr -d '"')
            matnum=$(echo $line | awk -F "${csvsep}" '{printf "%s", $4}' | tr -d '"')
        fi

        #lastname=$(echo "${lastname// /-}")
        dest="${outfolder}/${matnum}_${lastname:0:1}.pdf"
        if ! [[ "$dry" = "y" ]]; then
            cp ${longpdffile} ${dest}
        else
            dryoutput="${dryoutput}\n${longpdffile}: ${dest}"
        fi
        
        # Progress
        if ! ((cnt % ((numlines+10)/10))); then
            echo -ne "."
        fi
        let "cnt+=1"
    done 
} < "${csv}"

echo -ne "done."


# Echo dry output
if  [[ "$dry" = "y" ]]; then
    echo -e "\n\nResults from dry run:\n${dryoutput}"
fi

echo
echo "Done."
echo

exit
